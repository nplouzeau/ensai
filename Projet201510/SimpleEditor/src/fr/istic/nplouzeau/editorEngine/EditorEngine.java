package fr.istic.nplouzeau.editorEngine;

/**
 * Created by plouzeau on 2015-10-01.
 */
public interface EditorEngine {
    /**
     * Removes the selection's content and puts it into the clipboard.
     */
    void cut();

    /**
     * Copies the selection's contents into the clipboard.
     */
    void copy();

    /**
     * Replaces the selection's contents with the contents of the clipboard.
     */
    void paste();

    /**
     * Changes the selection's position.
     * @param start
     * @param length
     */
    void setSelection(int start, int length);

    /**
     * Replaces the selection's contents with the 'text'
     * @param text
     */
    void insert(String text);

    /**
     * Sends the contents of the editor's buffer to a R interpreter.
     */
    void evaluate();
}
