package fr.istic.nplouzeau.editorEngine;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by plouzeau on 2015-10-01.
 */
public class SimpleEditor extends Application {

    private Controller controller;
    private EditorEngine editorEngine;

    @Override
    public void start(Stage primaryStage) throws Exception {

        editorEngine = new DummyEditorEngineImpl();
        controller = new Controller(primaryStage,editorEngine);
        controller.buildGUI();
        controller.run();
    }

    public static void main(String[] args) {
        launch(args);
    }
}






