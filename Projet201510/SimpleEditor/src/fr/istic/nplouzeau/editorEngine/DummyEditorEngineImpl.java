package fr.istic.nplouzeau.editorEngine;

/**
 * Created by plouzeau on 2015-10-02.
 */
public class DummyEditorEngineImpl implements EditorEngine {
    /**
     * Removes the selection's content and puts it into the clipboard.
     */
    @Override
    public void cut() {

    }

    /**
     * Copies the selection's contents into the clipboard.
     */
    @Override
    public void copy() {

    }

    /**
     * Replaces the selection's contents with the contents of the clipboard.
     */
    @Override
    public void paste() {

    }

    /**
     * Changes the selection's position.
     *
     * @param start
     * @param length
     */
    @Override
    public void setSelection(int start, int length) {

    }

    /**
     * Replaces the selection's contents with the 'text'
     *
     * @param text
     */
    @Override
    public void insert(String text) {

    }

    /**
     * Sends the contents of the editor's buffer to a R interpreter.
     */
    @Override
    public void evaluate() {

    }
}
