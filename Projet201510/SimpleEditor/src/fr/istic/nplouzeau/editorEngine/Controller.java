package fr.istic.nplouzeau.editorEngine;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.logging.Logger;

/**
 * Created by plouzeau on 2015-10-01.
 */
public class Controller {

    private Stage primaryStage;
    private EditorEngine engine;
    private BorderPane root;

    private TextArea inputArea;
    private TextArea outputArea;

    public Controller(Stage primaryStage, EditorEngine engine) {
        this.primaryStage = primaryStage;
        this.engine = engine;


    }

    public void buildGUI() {
        root = new BorderPane();
        buildMenus();
        buildTexts();

        primaryStage.setTitle("SimpleEditor");
        primaryStage.setScene(new Scene(root, 300, 275));
    }

    private void buildTexts() {
        inputArea = new TextArea();
        inputArea.setText("To be edited");
        inputArea.setEditable(true);
        inputArea.selectionProperty().addListener((obsValue,oldRange,newRange) -> getSelection(newRange));
        inputArea.setOnKeyTyped(keyEvent -> getTypedKey(keyEvent));

        outputArea = new TextArea();
        outputArea.setText("Results go here");
        outputArea.setEditable(false);

        root.setCenter(inputArea);
        root.setBottom(outputArea);

    }

    private void getTypedKey(KeyEvent keyEvent) {
        Logger.getGlobal().info(keyEvent.getCharacter());
        engine.insert(keyEvent.getCharacter());
    }

    private void getSelection(IndexRange newRange) {
        Logger.getGlobal().info(String.format("Start %d, end %d",newRange.getStart(),newRange.getLength()));
        engine.setSelection(newRange.getStart(),newRange.getLength());
    }

    private void buildMenus() {
        MenuBar menuBar = new MenuBar();
        Menu editMenu = new Menu("Edit");
        // Add a menu item to cut
        MenuItem addMenuItem = new MenuItem("Cut");

        addMenuItem.setOnAction(event -> this.cut());
        editMenu.getItems().add(addMenuItem);

        // Ditto for copy
        MenuItem removeMenuItem = new MenuItem("Copy");
        editMenu.getItems().add(removeMenuItem);
        removeMenuItem.setOnAction(event -> this.copy());


        // Ditto for paste
        MenuItem pasteMenuItem = new MenuItem("Paste");
        editMenu.getItems().add(pasteMenuItem);
        pasteMenuItem.setOnAction(event -> this.paste());


        // Ditto for evaluate
        MenuItem evaluateMenuItem = new MenuItem("Evaluate");
        editMenu.getItems().add(evaluateMenuItem);
        evaluateMenuItem.setOnAction(event -> this.evaluate());

        // Add the Edit menu
        menuBar.getMenus().add(editMenu);
        root.setTop(menuBar);
    }

    private void evaluate() {
        engine.evaluate();
    }

    private void paste() {
        engine.paste();
    }

    private void copy() {
        engine.copy();
    }

    private void cut() {
        engine.cut();
    }

    public void run() {
        primaryStage.show();

    }
}
